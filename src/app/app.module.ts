import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { CaptchaService } from "./services/captcha.service";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpModule, HttpClientModule, FormsModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [CaptchaService],
  bootstrap: [AppComponent]
})
export class AppModule {}
