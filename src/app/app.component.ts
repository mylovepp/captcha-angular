import { Component } from "@angular/core";
import { CaptchaService } from "./services/captcha.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "captcha-angular";
  captchaResult = undefined;
  captchaImage = undefined;
  captchaText = "";
  captchaValidationResult = undefined;

  constructor(
    private sanitizer: DomSanitizer,
    private service: CaptchaService
  ) {}

  ngOnInit() {
    this.getCaptcha();
  }
  getCaptcha = (): void => {
    this.service.getCaptcha().subscribe(
      response => {
        console.log("response", response);
        this.captchaResult = response;
        this.captchaImage = this.sanitizer.bypassSecurityTrustHtml(
          this.captchaResult.captcha
        );
      },
      error => {
        console.log("error", error);
      }
    );
  };

  validateCaptcha = (): void => {
    const capptcharToken = {
      captchaId: this.captchaResult.captchaId,
      captchaText: this.captchaText
    };

    this.service.validateCaptcha(capptcharToken).subscribe(
      response => {
        console.log("response", response);
        this.captchaValidationResult = response["message"];
      },
      error => {
        console.log("error", error);
      }
    );
  };
}
