import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class CaptchaService {
  constructor(private httpClient: HttpClient) {}

  public getCaptcha() {
    return this.httpClient.get("http://localhost:30001/captcha");
  }

  public validateCaptcha(captchaToken) {
    return this.httpClient.post("http://localhost:30001/captcha", captchaToken);
  }
}
